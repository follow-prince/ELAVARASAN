import blog1 from "../assets/images/repositories/UC/UC-1.png";
import blog2 from "../assets/images/repositories/data/data-1.png";
import blog3 from "../assets/images/repositories/ec-counsole/ec-counsole-1.png";
import portfolio from "../assets/images/repositories/google-cloud/google-cloud-1.png";
import portfoliov2 from "../assets/images/repositories/meta/meta-1.png";
import quickstart from "../assets/images/repositories/meta-front/meta-front-1.png";
import cardportfolio from "../assets/images/repositories/nyu/nyu-1.png";
import imgGallery from "../assets/images/repositories/solo/solo-1.png";
import notebook from "../assets/images/repositories/solo2222/solo2222-1.png";
import devto from "../assets/images/repositories/sql_advanced certificate/sql_advanced certificate-1.png";

export const repositories = [
  {
    title: "The Full Stack",
    description: " ",
    cover: portfoliov2,
    technologies: ["Django", "React", "Javascript"],
    url: "https://www.coursera.org/verify/YAW7G2QESQ6Q",
    blurHash: "L25#he^nryxc^-w$V{V_56bqx[M{",


  },
 
  {
    title: "Java Script - Intermediate",
    description:
      "  ",
    cover: notebook,
    technologies: ["javascript"],
    url: "https://www.hackerrank.com/certificates/ffd3144f56f6",
    blurHash: "LMOWg4wIS$WBH?sAkCR*?^bcnNoy",
    stars: "54",
    fork: "5"
  },
  {
    title: "SQL - Advanced ",
    description:
      " ",
    cover: devto,
    technologies: ["Mysql", "MongoDB", "FireBase"],
    url: "https://www.hackerrank.com/certificates/8be3aea1b386",
    blurHash: "L4S~x5xb~q$*~WR6MykCx^Vt4TtP",
    stars: "18",
    fork: "7"
  },
  {
    title: "Launching into Machine Learning",
    description: " ",
    cover: portfolio,
    technologies: ["GoogleCloudPlatform", "Bigquery", "Inuclusive ML "],
    url: "https://www.coursera.org/verify/FLW6QZQAE8DT",
    blurHash: "L35O{d~XeoMyDhRPxv%Ms=xunmRQ",
    stars: "16",
    fork: "6"
  },
  {
    title: "Animation with Javascript & JQuery",
    description:
      " ",
    technologies: ["animations", "Jquery", "JavaScript"],
    cover: blog1,
    url: "https://www.coursera.org/verify/FX4TT6VBJYN8",
    blurHash: "LMMaw^IV~pxu00%LRjNGAIIVadt6",
    stars: "13",
    fork: "2"
  },
  {
    title: "Foundations: Data, Data, Everywhere",
    description: " ",
    cover: blog2,
    technologies: ["DataAnalysis", "Spreadsheet", "Data Cleansing"], 
    url: "https://www.coursera.org/verify/H5HVDXANUBFF",
    blurHash: "LSN1AcH?~Wtl00={M{NG0eIoj]xa",
    stars: "12",
    fork: '3'
  },
  {
    title: "Introduction to Cyber Attacks",
    description: " ",
    cover: cardportfolio,
    technologies: ["DOS", "INFOSEC", "Cybersecurity"],
    url: "https://coursera.org/verify/UM3T47JNR4VJ",
    blurHash: "L15#hiax00og^Uf65Nj]oPfkWRf6",
    stars: "9",
    fork: "2"
  },
  {
    title: "Front-End Development",
    description: " ",
    cover: quickstart,
    technologies: ["Responsive Web Design", "UserInterface"],
    url: "https://www.coursera.org/verify/HTJFT8VS9W45",
    blurHash: "LLPGmg?wRPS~D%D%ozxa00?G-;R%",
    stars: "8"
  },
  {
    title: "Secure Full Stack MEAN Developer",
    description: " ",
    cover: blog3,
    technologies: ["MEAN Stack Development", "Dependency Injection System"],
    url: "https://www.coursera.org/verify/XGRYNK6X6UQG",
    blurHash: "L6O;6=00M|og00_4_4s:E9-oxVoL",
    stars: "4"
  },
  {
    title: "HTML",
    description:
      " ",
    cover: imgGallery,
    technologies: ["html"],
    url: "https://www.sololearn.com/certificates/CT-2UB4KTAZ",
    blurHash: "LMOWg4wIS$WBH?sAkCR*?^bcnNoy",
    stars: "3"
  },
 

];
